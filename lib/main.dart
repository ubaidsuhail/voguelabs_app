import 'package:voguelabs/routes.dart';
import 'package:voguelabs/screens/app_intro/app_intro_screen.dart';
import 'package:voguelabs/screens/launch_screen/launchscreen.dart';
import 'package:voguelabs/screens/splash/splash_screen.dart';
import 'package:voguelabs/theme.dart';
import 'package:voguelabs/util.dart';
import 'package:voguelabs/widgets/app_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app_localizations.dart';
import 'delegates/app_localizations_delegate.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return AppBuilder(
      builder: (context) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          routes: routes,
          theme: theme(),
          supportedLocales: [
            Locale('en', 'US'),
            Locale('ar', ''),
          ],
          locale: Utils.appLocale,
          localizationsDelegates: [
            const AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          localeResolutionCallback: (locale, supportedLocales) {
            for (var supportedLocaleLanguage in supportedLocales) {
              print(supportedLocaleLanguage.languageCode);
              if (supportedLocaleLanguage.languageCode == locale.languageCode &&
                  supportedLocaleLanguage.countryCode == locale.countryCode) {
                return supportedLocaleLanguage;
              }
            }
            // If device not support with locale to get language code then default get first on from the list
            return supportedLocales.first;
          },
          title: 'VOGUE Labs',
          initialRoute: LaunchScreen.routeName,
        );
      },
    );
  }
}
