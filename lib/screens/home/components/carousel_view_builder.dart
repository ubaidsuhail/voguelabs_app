import 'package:voguelabs/constant.dart';
import 'package:voguelabs/widgets/carousel_pro.dart';
import 'package:flutter/material.dart';

class CarouselViewBuilder extends StatefulWidget {
  @override
  _CarouselViewBuilderState createState() => _CarouselViewBuilderState();
}

class _CarouselViewBuilderState extends State<CarouselViewBuilder> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      height: 182.0,
      child: new Carousel(
        borderRadius: true,
        boxFit: BoxFit.cover,
        radiusDouble: 10,
        radius: Radius.circular(10.0),
        dotColor: kWhiteColor,
        dotSize: 5.5,
        dotSpacing: 16.0,
        dotBgColor: Colors.transparent,
        showIndicator: true,
        overlayShadow: true,
        overlayShadowSize: 0.9,
        images: [
          AssetImage("assets/images/category_shoes.jpg"),
          AssetImage("assets/images/category_camera.jpeg"),
          AssetImage("assets/images/category_electronics.jpeg"),
          AssetImage("assets/images/category_clothes.jpeg"),
          AssetImage("assets/images/category3.png"),
        ],
      ),
    );
  }
}
