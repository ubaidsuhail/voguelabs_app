import 'package:voguelabs/app_localizations.dart';
import 'package:voguelabs/constant.dart';
import 'package:voguelabs/screens/all_product/all_product_screen.dart';
import 'package:voguelabs/screens/all_product/components/all_product_body.dart';
import 'package:voguelabs/screens/category_detail/category_detail_screen.dart';
import 'package:voguelabs/screens/home/components/carousel_view_builder.dart';
import 'package:voguelabs/screens/home/components/category_list_builder.dart';
import 'package:voguelabs/screens/home/components/grid_item_list.dart';
import 'package:voguelabs/util.dart';
import 'package:voguelabs/widgets/carousel_pro.dart';
import 'package:flutter/material.dart';

import 'header_title.dart';
import 'item_list_builder.dart';

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Utils.isDarkMode ? kDarkColor : kWhiteColor,
      child: ListView(
        shrinkWrap: true,
        children: [
          HeaderTitle(
              ApplicationLocalizations.of(context).translate("categories"),
              ApplicationLocalizations.of(context).translate("view_all"),
              Utils.isDarkMode ? kDarkBlackFontColor : kLightBlackTextColor,
              () {
            Navigator.of(context).pushNamed(CategoryDetailScreen.routeName);
          }),
          CateogryListBuilder(),
          CarouselViewBuilder(),
          HeaderTitle(
              ApplicationLocalizations.of(context).translate("best_seller"),
              ApplicationLocalizations.of(context).translate("view_all"),
              Utils.isDarkMode ? kDarkBlackFontColor : kLightBlackTextColor,
              () {
            Navigator.of(context).pushNamed(AllProductItemScreen.routeName);
          }),
          ItemListBuilder(),
          HeaderTitle(
              ApplicationLocalizations.of(context).translate("newest"),
              ApplicationLocalizations.of(context).translate("view_all"),
              Utils.isDarkMode ? kDarkBlackFontColor : kLightBlackTextColor,
              () {
            Navigator.of(context).pushNamed(AllProductItemScreen.routeName);
          }),
          GridListBuilder(),
        ],
      ),
    );
  }
}
