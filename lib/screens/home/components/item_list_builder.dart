import 'package:voguelabs/models/product_item_model.dart';
import 'package:voguelabs/screens/product_detail/product_detail_screen.dart';
import 'package:flutter/material.dart';

import 'product_item_builder.dart';

class ItemListBuilder extends StatefulWidget {
  @override
  _ItemListBuilderState createState() => _ItemListBuilderState();
}

class _ItemListBuilderState extends State<ItemListBuilder> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0),
      child: SizedBox(
        height: 310,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Hero(
              tag: "hero-Item-${productList[index].id}",
              child: Material(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ProductDetailScreen(productList[index])));
                  },
                  child: FittedBox(
                    child: ProductItemBuilder(
                      isDiscount: true,
                      productItem: productList[index],
                    ),
                  ),
                ),
              ),
            );
          },
          itemCount: productList.length,
        ),
      ),
    );
  }
}
