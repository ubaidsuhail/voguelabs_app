import 'package:voguelabs/models/brand_model.dart';
import 'package:voguelabs/models/category_models.dart';
import 'package:voguelabs/screens/brand_detail/brand_detail_screen.dart';
import 'package:voguelabs/screens/home/components/category_item_builder.dart';
import 'package:flutter/material.dart';

import '../../../constant.dart';
import '../../../util.dart';

class CateogryListBuilder extends StatefulWidget {
  @override
  _CateogryListBuilderState createState() => _CateogryListBuilderState();
}

class _CateogryListBuilderState extends State<CateogryListBuilder> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Hero(
            tag: 'hero-tag-${brandData[index].id}',
            child: Material(
              color: Utils.isDarkMode ? kDarkDefaultBgColor : kDefaultBgColor,
              child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                BrandDetailScreen(brandData[index])));
                  },
                  child: CategoryListItemBuilder(brandData[index])),
            ),
          );
        },
        itemCount: brandData.length,
      ),
    );
  }
}
