import 'package:voguelabs/app_localizations.dart';
import 'package:voguelabs/constant.dart';
import 'package:voguelabs/delegates/app_localizations_delegate.dart';
import 'package:voguelabs/screens/search/search_screen.dart';
import 'package:voguelabs/widgets/normal_text.dart';
import 'package:flutter/material.dart';

import '../../theme.dart';
import '../../util.dart';
import 'components/home_body.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = "/home_screen";
  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    return Scaffold(
        backgroundColor:
            Utils.isDarkMode ? kDarkDefaultBgColor : kDefaultBgColor,
        resizeToAvoidBottomInset: false,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(110.0),
          child: SafeArea(
            child: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor:
                  Utils.isDarkMode ? kDarkDefaultBgColor : kDefaultBgColor,
              centerTitle: false,
              title: Column(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "VOGUE Labs",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Utils.isDarkMode
                                  ? kDarkTextColorColor
                                  : kAppColor,
                              fontWeight: FontWeight.w600,
                              fontSize: kLargeFontSize,
                              fontFamily: 'Arial',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 2.0),
                          child: Text(
                            ApplicationLocalizations.of(context)
                                .translate("delly_shop_desc"),
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: kLightBlackTextColor,
                                fontWeight: FontWeight.w600,
                                fontSize: kMicroFontSize),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              bottom: PreferredSize(
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed(SearchScreen.routeName);
                  },
                  child: Container(
                    height: 40,
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.3),
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        shape: BoxShape.rectangle),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.search,
                            color: Colors.black38,
                          ),
                        ),
                        NormalTextWidget(
                            ApplicationLocalizations.of(context)
                                .translate("search"),
                            Utils.isDarkMode
                                ? kDarkTextColorColor
                                : kLightBlackTextColor,
                            kSmallFontSize)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        body: HomeBody());
  }
}
