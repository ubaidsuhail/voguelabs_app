import 'dart:async';

import 'package:flutter/material.dart';
import 'package:voguelabs/screens/app_intro/app_intro_screen.dart';

import '../../constant.dart';



class LaunchScreen extends StatefulWidget {
  static const routeName = "launchscreen";
  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    MoveToNextScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Spacer(),
          Image(image: AssetImage("assets/images/appnewlogo.png"),
            //width: 220.0,
          ),
          Spacer(),
          Text(
            "www.vogue-labs.com",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                fontSize: kTitleFontSize),
          ),

          SizedBox(height:2.0),

          Text(
            "VOGUE Labs Cosmetics LLC",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                fontSize: kTitleFontSize),
          ),

          SizedBox(height:4.0)
        ],
      ),

    );
  }


  void MoveToNextScreen()
  {

    Timer(
      Duration(seconds: 3),
          () => Navigator.push(
        context,
        PageRouteBuilder(
          transitionDuration: Duration(seconds:4),
          pageBuilder: (context, __, ___) => AppIntroScreen(),
        ),
      ),
    );

  }
}
