import 'package:voguelabs/constant.dart';
import 'package:voguelabs/models/notification_model.dart';
import 'package:voguelabs/widgets/custom_scaffold.dart';
import 'package:voguelabs/widgets/normal_text.dart';
import 'package:flutter/material.dart';

import 'components/notification_detail_body.dart';

class NotificationDetailScreen extends StatelessWidget {
  static const routeName = "/notification_detail_screen";
  NotificationModel _notificationModel;
  NotificationDetailScreen(this._notificationModel);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NotificationBody(_notificationModel),
    );
  }
}
