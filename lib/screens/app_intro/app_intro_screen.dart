import 'package:voguelabs/constant.dart';
import 'package:voguelabs/screens/splash/splash_screen.dart';
import 'package:voguelabs/widgets/normal_text.dart';
import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

class AppIntroScreen extends StatefulWidget {
  static const routeName = "app_intro_screen";
  @override
  _AppIntroScreenState createState() => _AppIntroScreenState();
}

class _AppIntroScreenState extends State<AppIntroScreen> {
  @override
  Widget build(BuildContext context) {
    return IntroViewsFlutter(
      [page, page1, page2],
      onTapDoneButton: () {
        Navigator.of(context).pushReplacementNamed(SplashScreen.routeName);
      },
      showSkipButton: true,
      pageButtonTextStyles: new TextStyle(
        color: Colors.white,
        fontSize: 18.0,
      ),
    );
  }

  var page = new PageViewModel(
    pageColor: startPageColor,
    //pageColor: startPageColor,
    iconColor: null,
    bubbleBackgroundColor: Colors.white,
    body: Text(
      'Choose from Thousands of Products',
    ),
    title: NormalTextWidget('Thousands of Products', Color(0XFF587c94), kPriceFontSize),
    mainImage: Image.asset(
      'assets/images/splash_image1.png',
      height: 285.0,
      width: 285.0,
      //alignment: Alignment.center,
    ),
    titleTextStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
    bodyTextStyle: TextStyle(color: Color(0XFF587c94)),
  );
  var page1 = new PageViewModel(
    pageColor: startPageColor,
    iconColor: null,
    bubbleBackgroundColor: Colors.white,
    body: Text(
      'Review the products you selected in the basket',
    ),
    title: NormalTextWidget('Check Products', Color(0XFF587c94), kPriceFontSize),
    mainImage: Image.asset(
      'assets/images/splash_image2.png',
      height: 285.0,
      width: 285.0,
      alignment: Alignment.center,
    ),
    titleTextStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
    bodyTextStyle: TextStyle(color: Color(0XFF587c94)),
  );
  var page2 = new PageViewModel(
    //pageColor: Color(0XFFd5f1fc),
    pageColor:startPageColor,
    iconColor: null,
    bubbleBackgroundColor: Colors.white,
    body: Text(
      'Order the Products you want now',
    ),
    title:
        NormalTextWidget('Complete Your Order', Color(0XFF587c94), kPriceFontSize),
    mainImage: Image.asset(
      'assets/images/splash_image3.png',
      height: 285.0,
      width: 285.0,
      alignment: Alignment.center,
    ),
    titleTextStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
    bodyTextStyle: TextStyle(color: Color(0XFF587c94)),
  );
}
